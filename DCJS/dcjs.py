#!/usr/bin/python
#coding=utf-8
import sys
import math


InputNum=len(sys.argv)
InputX=sys.argv

if(InputNum==3):
    N1=float(sys.argv[1])
    N2=float(sys.argv[2])
    JSJG=math.log(N2)/math.log(N1)
    print("细胞密度1：%.2f" % N1)
    print("细胞密度2：%.2f" % N2)
    print("细胞代次计算结果：%.2f" % JSJG)
else:
    if(InputNum<3):
        print("需要输入2个数值！")
    else:
        print("输入了太多数值！")
