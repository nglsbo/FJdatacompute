<!DOCTYPE html>
<html>
<head>
</head>
<body>
  <div id="container" style="width:1000px">
    <div id="header" style="background-color:#F5F5F5;">
      <img src="../FJcom.gif"/>
    </div>
    <div id="menu" style="background-color:#E0EEEE;height:30px;width:1000px;">
      <div style="float:left;">现在位置：绘制生长曲线图->数据计算结果
      </div>
      <div style="float:right;"><a href="../../index.html">返回首页</a>
      </div>
    </div>
    <div id="content" style="background-color:#F5F5F5;height:800px;width:1000px;">
      <p>数据计算结果：
      </p>	
      <?php
	$SJ1=$_POST['SJ1'];
	$SJ2=$_POST['SJ2'];
	$SJ3=$_POST['SJ3'];
	$SJ4=$_POST['SJ4'];
	$SJ5=$_POST['SJ5'];
	$SJ6=$_POST['SJ6'];
	$SJ7=$_POST['SJ7'];
	$SJ8=$_POST['SJ8'];
	$SJ9=$_POST['SJ9'];
	$SJ10=$_POST['SJ10'];
	$N1=$_POST['N1'];
	$N2=$_POST['N2'];
	$N3=$_POST['N3'];
	$N4=$_POST['N4'];
	$N5=$_POST['N5'];
	$N6=$_POST['N6'];
	$N7=$_POST['N7'];
	$N8=$_POST['N8'];
	$N9=$_POST['N9'];
	$N10=$_POST['N10'];
	$v1=($N2-$N1)/($SJ2-$SJ1);
	$v2=($N3-$N2)/($SJ3-$SJ2);
	$v3=($N4-$N3)/($SJ4-$SJ3);
	$v4=($N5-$N4)/($SJ5-$SJ4);
	$v5=($N6-$N5)/($SJ6-$SJ5);
	$v6=($N7-$N6)/($SJ7-$SJ6);
	$v7=($N8-$N7)/($SJ8-$SJ7);
	$v8=($N9-$N8)/($SJ9-$SJ8);
	$v9=($N10-$N9)/($SJ10-$SJ9);
	$U1=$v1/$N1;
	$U2=$v2/$N2;
	$U3=$v3/$N3;
	$U4=$v4/$N4;
	$U5=$v5/$N5;
	$U6=$v6/$N6;
	$U7=$v7/$N7;
	$U8=$v8/$N8;
	$U9=$v9/$N9;
      ?>
      <?php
	$servername="127.0.0.1";
	$username="root";
	$password="mysql854596";
	$dbname="FJdata";
	$conn = new mysqli($servername,$username,$password,$dbname);
	if(! $conn){
		die("连接失败");
	}
	else{
		$sql="DELETE FROM data_tb1";
		$retval=mysqli_query($conn,$sql);
		if(! $retval){
		  	echo "delete success";
		}
		mysqli_select_db($conn,'data_tb1');
			
			$sql="INSERT INTO data_tb1(data_TIME,data_OD600,data_NUM)
			VALUES('$SJ1','$N1','1'),	
			('$SJ2','$N2','2'),	
			('$SJ3','$N3','3'),	
			('$SJ4','$N4','4'),	
			('$SJ5','$N5','5'),	
			('$SJ6','$N6','6'),	
			('$SJ7','$N7','7'),	
			('$SJ8','$N8','8'),	
			('$SJ9','$N9','9'),	
			('$SJ10','$N10','10')";	

			$retval=mysqli_query($conn,$sql);
			if( ! $retval){
				echo "insert success";
			}
			mysqli_close($conn);
	}
      ?>
      <table border="1">
        <tr>
          <td>阶段</td>
          <td>生长速率</td>
          <td>比生长速率</td>
        </tr>
        <tr>
          <td>1-2</td>
          <td><?php printf("%.2f",$v1)?></td>
          <td><?php printf("%.2f",$U1)?></td>
        </tr>
        <tr>
          <td>2-3</td>
          <td><?php printf("%.2f",$v2)?></td>
          <td><?php printf("%.2f",$U2)?></td>
        </tr>
        <tr>
          <td>3-4</td>
          <td><?php printf("%.2f",$v3)?></td>
          <td><?php printf("%.2f",$U3)?></td>
        </tr>
        <tr>
          <td>4-5</td>
          <td><?php printf("%.2f",$v4)?></td>
          <td><?php printf("%.2f",$U4)?></td>
        </tr>
        <tr>
          <td>5-6</td>
          <td><?php printf("%.2f",$v5)?></td>
          <td><?php printf("%.2f",$U5)?></td>
        </tr>
        <tr>
          <td>6-7</td>
          <td><?php printf("%.2f",$v6)?></td>
          <td><?php printf("%.2f",$U6)?></td>
        </tr>
        <tr>
          <td>7-8</td>
          <td><?php printf("%.2f",$v7)?></td>
          <td><?php printf("%.2f",$U7)?></td>
        </tr>
        <tr>
          <td>8-9</td>
          <td><?php printf("%.2f",$v8)?></td>
          <td><?php printf("%.2f",$U8)?></td>
        </tr>
        <tr>
          <td>9-10</td>
          <td><?php printf("%.2f",$v9)?></td>
          <td><?php printf("%.2f",$U9)?></td>
        </tr>
      </table>
      <a href="image5.php">生成图像</a>
    </div>
  </div>
</body>
</html>
